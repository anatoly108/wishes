package com.wishesapp;


public abstract class Constants {
    public static String wishId = "wishId";
    public static String wishRingtone = "wishRingtone";
    public static String reminderId = "reminderId";
    public static String wishText = "wishText";
    public static String channelId = "wishesChannel";
    public static String allIdsPref = "allIdsPref";
    public static String reminderIdKey = "reminderIdKey";


    public static int reservoirSize = 20 * 1000 * 1000;
}
