package com.wishesapp;

import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.wishesapp.activities.SetupWishActivity;
import com.wishesapp.models.Reminder;
import com.wishesapp.models.Wish;

import java.util.List;

import static android.content.Context.ALARM_SERVICE;

class RemindersViewHolder extends RecyclerView.ViewHolder {
    TextView reminderTime;
    private FragmentManager fragmentManager;
    public TimePickedListener timePickedListener;
    public DialogInterface.OnClickListener onClickListener;

    RemindersViewHolder(View itemView, FragmentManager fragmentManager, Wish wish) {
        super(itemView);
        reminderTime = itemView.findViewById(R.id.reminder_time);

        this.fragmentManager = fragmentManager;
    }

    void bind(final Reminder item) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                TimePickerFragment newFragment = new TimePickerFragment();
                newFragment.setOnClickListener(onClickListener);
                newFragment.setTimePickedListener(timePickedListener);
                newFragment.show(fragmentManager, "timePicker");
            }
        });
    }
}

public class RemindersAdapter extends RecyclerView.Adapter<RemindersViewHolder> {
    private List<Reminder> reminders;
    private FragmentManager fragmentManager;
    private SetupWishActivity setupWishActivity;
    private Wish wish;

    public RemindersAdapter(List<Reminder> wishes, FragmentManager fragmentManager, SetupWishActivity setupWishActivity, Wish wish) {
        this.reminders = wishes;
        this.fragmentManager = fragmentManager;
        this.setupWishActivity = setupWishActivity;
        this.wish = wish;
    }

    @NonNull
    @Override
    public RemindersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item,
                parent,
                false);
        return new RemindersViewHolder(itemView, fragmentManager, wish);
    }

    @Override
    public void onBindViewHolder(@NonNull RemindersViewHolder holder, int position) {
        final Reminder reminder = reminders.get(position);
        holder.reminderTime.setText(reminder.getTime());
        holder.bind(reminder);

        holder.timePickedListener = new TimePickedListener() {
            @Override
            public void timePickedEvent(int hourOfDay, int minute) {
                reminder.setHour(hourOfDay);
                reminder.setMinute(minute);
                setupWishActivity.reloadReminders();
            }
        };

        holder.onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // removing reminder

                // 1. cancel alarm
                AlarmManager alarmManager = (AlarmManager) setupWishActivity.getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(setupWishActivity, AlarmReceiver.class);

                PendingIntent oldPendingIntent = PendingIntent.getBroadcast(setupWishActivity.getApplicationContext(),
                            (int) reminder.getId(),
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.cancel(oldPendingIntent);

                // 2. delete reminder itself
                wish.removeReminder(reminder);

                setupWishActivity.reloadReminders();
            }
        };
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
