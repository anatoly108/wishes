package com.wishesapp;

public interface TimePickedListener {
    void timePickedEvent(int hourOfDay, int minute);
}
