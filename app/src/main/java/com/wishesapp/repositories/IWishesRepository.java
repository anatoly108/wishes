package com.wishesapp.repositories;

import com.wishesapp.models.Wish;

import java.util.List;
import java.util.UUID;

public interface IWishesRepository {
    Wish get(UUID id);
    List<Wish> getWishes();
    void saveWish(Wish wish);
    void deleteWish(UUID id);
}
