package com.wishesapp.repositories;

import android.content.Context;
import android.content.SharedPreferences;
import com.anupcowkur.reservoir.Reservoir;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wishesapp.Constants;
import com.wishesapp.WishFactory;
import com.wishesapp.models.Wish;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JsonWishesRepository implements IWishesRepository {

    private SharedPreferences prefs;

    public JsonWishesRepository(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public JsonWishesRepository(SharedPreferences prefs, String defaultWishString, Context context) {
        this.prefs = prefs;

        if (getWishesIds().isEmpty()){
            Wish wish = WishFactory.CreateDefaultWish(prefs, defaultWishString);
            saveWish(wish);
            return;
        }

        if (getWishes().size() != 0)
            return;

        // it means that App has something in Reservoir
        try {
            Reservoir.init(context, Constants.reservoirSize); //in bytes
        } catch (IOException e) {
            //failure
            e.printStackTrace();
            return;
        }

        // get everything from Reservoir and put it into prefs
        for (String wishId : getWishesIds()) {
            try {
                Wish wish = Reservoir.get(wishId, Wish.class);
                saveWish(wish);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Wish get(UUID id) {
        Gson gson = new Gson();
        String json = prefs.getString(id.toString(), null);
        if (json == null)
            return null;

        Type type = new TypeToken<Wish>() {}.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public List<Wish> getWishes(){
        ArrayList<String> allIds = getWishesIds();
        ArrayList<Wish> wishes = new ArrayList<>();
        for (String wishId : allIds) {
            Wish wish = get(UUID.fromString(wishId));
            if (wish == null)
                continue;
            wishes.add(wish);
        }

        return wishes;
    }

    @Override
    public void saveWish(Wish wish) {
        if (wish.getId() == null){
            wish.setId(UUID.randomUUID());

            ArrayList<String> allIds = getWishesIds();
            if (allIds == null){
                allIds = new ArrayList<>();
            }
            allIds.add(wish.getId().toString());
            putToPrefs(Constants.allIdsPref, allIds);
        }

        putToPrefs(wish.getId().toString(), wish);
    }

    @Override
    public void deleteWish(UUID id) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(id.toString());
        editor.apply();

        ArrayList<String> allIds = getWishesIds();
        allIds.remove(id.toString());
        putToPrefs(Constants.allIdsPref, allIds);
    }

    private ArrayList<String> getWishesIds(){
        Gson gson = new Gson();
        String json = prefs.getString(Constants.allIdsPref, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> result = gson.fromJson(json, type);
        if(result != null)
            return result;
        else
            return new ArrayList<>();
    }

    private void putToPrefs(String key, Object object){
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(object);
        editor.putString(key, json);
        editor.apply();
    }
}
