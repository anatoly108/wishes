package com.wishesapp.repositories;


import com.wishesapp.RemindersFactory;
import com.wishesapp.models.Reminder;
import com.wishesapp.models.Wish;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MemoryWishesRepository implements IWishesRepository{
    private static ArrayList<Wish> wishes = new ArrayList<>();

    public MemoryWishesRepository() {
    }

    public MemoryWishesRepository(String defaultWishString) {
        if (wishes.size() > 0)
            return;

        ArrayList<Reminder> reminders1 = new ArrayList<>();
//        reminders1.add(RemindersFactory.Create(12, 0));
        wishes.add(new Wish(UUID.randomUUID(), defaultWishString, reminders1, false));
    }

    @Override
    public Wish get(UUID id) {
        for (Wish nextWish: wishes) {
            if (nextWish.getId() == id)
                return nextWish;
        }
        return null;
    }

    @Override
    public List<Wish> getWishes() {
        return wishes;
    }

    @Override
    public void saveWish(Wish wish) {
        for (Wish nextWish : wishes) {
            if (nextWish.getId() == wish.getId()){
                nextWish.setText(wish.getText());
                nextWish.setOn(wish.isOn());
                nextWish.setReminders(wish.getReminders());
                return;
            }
        }
        wish.setId(UUID.randomUUID());
        wishes.add(wish);
    }

    @Override
    public void deleteWish(UUID id) {
        int wishArrayIndex = -1;
        for (int i = 0; i < wishes.size(); i++){
            Wish wish = wishes.get(i);
            if (wish.getId() == id){
                wishArrayIndex = i;
                break;
            }
        }
        if (wishArrayIndex != -1)
            wishes.remove(wishArrayIndex);
    }
}
