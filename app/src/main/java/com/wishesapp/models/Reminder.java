package com.wishesapp.models;

import java.util.Locale;

public class Reminder {
    private long id;
    private int hour;
    private int minute;

    public Reminder(long id, int hour, int minute) {
        this.id = id;
        this.hour = hour;
        this.minute = minute;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getTime(){
        return String.format(Locale.US,"%02d:%02d", this.hour, this.minute);
    }
}
