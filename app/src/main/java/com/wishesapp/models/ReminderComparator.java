package com.wishesapp.models;

import java.util.Comparator;

public class ReminderComparator implements Comparator<Reminder> {

    @Override
    public int compare(Reminder o1, Reminder o2) {
        if (o1.getHour() > o2.getHour())
            return 1;

        if (o1.getHour() == o2.getHour() && o1.getMinute() > o2.getMinute())
            return 1;

        if (o1.getHour() == o2.getHour() && o1.getMinute() == o2.getMinute())
            return 0;

        return -1;
    }
}
