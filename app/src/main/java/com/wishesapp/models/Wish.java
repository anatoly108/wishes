package com.wishesapp.models;

import android.net.Uri;

import java.util.*;

public class Wish {
    private UUID id;
    private String text;
    private List<Reminder> reminders = new ArrayList<>();;
    private boolean isOn;
    private String ringtoneUri;

    public Wish() {
    }

    public Wish(UUID id, String text, List<Reminder> reminders, boolean isOn) {
        this.id = id;
        this.text = text;
        this.reminders = reminders;
        this.isOn = isOn;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void removeReminder(Reminder reminder){
        reminders.remove(reminder);
        Collections.sort(reminders, new ReminderComparator());
    }

    public void addReminder(Reminder reminder){
        reminders.add(reminder);
        Collections.sort(reminders, new ReminderComparator());
    }


    public String getRemindersString(String noRemindersString) {
        if (reminders.size() == 0){
            return noRemindersString;
        }
        StringBuilder result = new StringBuilder();

        for(int i = 0; i < reminders.size(); i++){
            Reminder reminder = reminders.get(i);
            result.append(reminder.getTime());
            if (i != reminders.size() - 1){
                result.append(", ");
            }
        }

        return result.toString();
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }

    public Uri getRingtoneUri() {
        return Uri.parse(ringtoneUri);
    }

    public void setRingtoneUri(Uri ringtoneUri) {
        this.ringtoneUri = ringtoneUri.toString();
    }
}
