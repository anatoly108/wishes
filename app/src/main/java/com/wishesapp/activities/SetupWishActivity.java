package com.wishesapp.activities;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.kevalpatel.ringtonepicker.RingtonePickerDialog;
import com.kevalpatel.ringtonepicker.RingtonePickerListener;
import com.wishesapp.*;
import com.wishesapp.models.Reminder;
import com.wishesapp.models.Wish;
import com.wishesapp.repositories.IWishesRepository;
import com.wishesapp.repositories.JsonWishesRepository;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class SetupWishActivity extends AppCompatActivity implements TimePickedListener {
    private WishUpdater wishUpdater;
    private Wish wish;
    private RecyclerView recyclerView;

    public SetupWishActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wish);
        Toolbar toolbar = findViewById(R.id.setup_wish_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        String wishIdString = intent.getStringExtra(Constants.wishId);

        if (wishIdString == null){
            finish();
        }

        UUID wishId = UUID.fromString(wishIdString);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        IWishesRepository wishesRepository = new JsonWishesRepository(prefs);

        wish = wishesRepository.get(wishId);

        recyclerView = findViewById(R.id.schedule_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        reloadReminders();

        TextView addTimeView = findViewById(R.id.add_time);
        final SetupWishActivity setupWishActivity = this;
        addTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment newFragment = new TimePickerFragment();
                newFragment.setTimePickedListener(setupWishActivity);
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        this.wishUpdater = new WishUpdater(wishesRepository, alarmManager, this.getApplicationContext(), this);

        // TODO: implement ringtone pick and sound
//        LinearLayout pickRingtone = findViewById(R.id.pick_ringtone);
//        pickRingtone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openPickRingtoneDialog();
//            }
//        });
//
//        updateRingtoneName();
    }

    private void updateRingtoneName() {
//        Ringtone ringtone = RingtoneManager.getRingtone(this, wish.getRingtoneUri());
//        String ringtoneTitle = ringtone.getTitle(this);
//        TextView ringtoneName = findViewById(R.id.wish_ringtone_name);
//        ringtoneName.setText(ringtoneTitle);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void timePickedEvent(int hourOfDay, int minute) {
        for (Reminder reminder : wish.getReminders()) {
            if (reminder.getHour() == hourOfDay && reminder.getMinute() == minute)
                return;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        wish.addReminder(RemindersFactory.Create(prefs, hourOfDay, minute));
        reloadReminders();
    }

    public void reloadReminders(){
        List<Reminder> reminders = wish.getReminders();
        RemindersAdapter remindersAdapter = new RemindersAdapter(reminders, getFragmentManager(), this, wish);
        recyclerView.setAdapter(remindersAdapter);
    }

    @Override
    public void onBackPressed() {
        wishUpdater.updateWishWithAlarms(wish, wish.isOn());

        super.onBackPressed();
    }

    private void openPickRingtoneDialog() {
        Uri ringtoneUri = null;
        if (wish.getRingtoneUri() == RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)){
            ringtoneUri = wish.getRingtoneUri();
        }
        RingtonePickerDialog.Builder ringtonePickerBuilder = new RingtonePickerDialog
                .Builder(this, getSupportFragmentManager())

        //Set title of the dialog.
        //If set null, no title will be displayed.
        .setTitle("Select ringtone")

        //set the currently selected uri, to mark that ringtone as checked by default.
        //If no ringtone is currently selected, pass null.
        .setCurrentRingtoneUri(ringtoneUri)

        //Set true to allow allow user to select default ringtone set in phone settings.
        .displayDefaultRingtone(true)

        //Set true to allow user to select silent (i.e. No ringtone.).
        .displaySilentRingtone(true)

        //set the text to display of the positive (ok) button.
        //If not set OK will be the default text.
        .setPositiveButtonText("SET RINGTONE")

        //set text to display as negative button.
        //If set null, negative button will not be displayed.
        .setCancelButtonText("CANCEL")

        //Set flag true if you want to play the sample of the clicked tone.
        .setPlaySampleWhileSelection(true)

        //Set the callback listener.
        .setListener(new RingtonePickerListener() {
            @Override
            public void OnRingtoneSelected(@NonNull String ringtoneName, Uri ringtoneUri) {
                //Do someting with selected uri...
                wish.setRingtoneUri(ringtoneUri);
                updateRingtoneName();
            }
        });

        //Add the desirable ringtone types.
        ringtonePickerBuilder.addRingtoneType(RingtonePickerDialog.Builder.TYPE_NOTIFICATION);

        //Display the dialog.
        ringtonePickerBuilder.show();
    }
}
