package com.wishesapp.activities;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.wishesapp.Constants;
import com.wishesapp.R;
import com.wishesapp.WishUpdater;
import com.wishesapp.WishesAdapter;
import com.wishesapp.models.Wish;
import com.wishesapp.repositories.IWishesRepository;
import com.wishesapp.repositories.JsonWishesRepository;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private IWishesRepository wishesRepository;
    private RecyclerView recyclerView;
    private WishUpdater wishUpdater;

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.wishesapp.R.layout.activity_main);
        createNotificationChannel();

        String fourImmeasurables = getResources().getString(R.string.four_immeasurables);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.wishesRepository = new JsonWishesRepository(prefs, fourImmeasurables, this);

        // cards
        recyclerView = findViewById(R.id.wishes_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        reloadWishes();

        FloatingActionButton newWishButton = findViewById(R.id.create_wish_button);
        final MainActivity mainActivity = this;
        newWishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editWishIntent = new Intent(mainActivity, EditWishActivity.class);
                mainActivity.startActivityForResult(editWishIntent, 1);
            }
        });

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        this.wishUpdater = new WishUpdater(wishesRepository, alarmManager, this.getApplicationContext(), this);
    }

    private void reloadWishes() {
        List<Wish> wishes = wishesRepository.getWishes();

        WishesAdapter wishesAdapter = new WishesAdapter(wishes,
            wishUpdater,
            this);
        recyclerView.setAdapter(wishesAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadWishes();
    }

    private void createNotificationChannel() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Constants.channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}

