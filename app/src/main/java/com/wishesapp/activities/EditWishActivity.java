package com.wishesapp.activities;

import android.app.AlarmManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import com.wishesapp.Constants;
import com.wishesapp.R;
import com.wishesapp.WishFactory;
import com.wishesapp.WishUpdater;
import com.wishesapp.models.Wish;
import com.wishesapp.repositories.IWishesRepository;
import com.wishesapp.repositories.JsonWishesRepository;

import java.util.Objects;
import java.util.UUID;

public class EditWishActivity extends AppCompatActivity {

    boolean isNew;
    private IWishesRepository wishesRepository;
    private Wish wish;
    private WishUpdater wishUpdater;
    private UUID wishId;

    public EditWishActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_wish);

        Toolbar toolbar = findViewById(R.id.edit_wish_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(Constants.wishId);
        isNew = stringExtra == null;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.wishesRepository = new JsonWishesRepository(prefs);

        final EditText wishEditText = findViewById(R.id.wish_edit_text);
        wishEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                wish.setText(text);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if(!isNew){
            wishId = UUID.fromString(stringExtra);
            loadWish();
            wishEditText.setText(wish.getText());
        } else {
            wish = WishFactory.Create();
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        this.wishUpdater = new WishUpdater(wishesRepository, alarmManager, this.getApplicationContext(), this);
    }

    private void loadWish() {
        if(wishId == null)
            return;
        wish = wishesRepository.get(wishId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_menu, menu);

        createAccentIcon(menu, R.id.delete_wish, R.drawable.ic_delete_black_24dp);
        createAccentIcon(menu, R.id.setup_wish, R.drawable.ic_settings_black_24dp);

        return true;
    }

    public void createAccentIcon(Menu menu, int itemId, int iconId){
        MenuItem item = menu.findItem(itemId);
        Drawable icon = getResources().getDrawable(iconId);
        icon.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        item.setIcon(icon);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_wish:
                if(!isNew){
                    wishUpdater.removeAlarms(wish);
                    wishesRepository.deleteWish(wish.getId());
                }

                Intent mainActivity = new Intent(this, MainActivity.class);
                setResult(1, mainActivity);
                finish();
                break;
            case R.id.setup_wish:
                wishesRepository.saveWish(wish);
                if (wishId != wish.getId())
                    wishId = wish.getId();

                Intent intent = new Intent( this, SetupWishActivity.class );
                intent.putExtra(Constants.wishId, wish.getId().toString());
                startActivity(intent);
                break;
            default:
                    break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(isTaskRoot()){
            wishUpdater.updateWishWithAlarms(wish, wish.isOn());
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (wish.getText() == null || wish.getText().equals("")){
            super.onBackPressed();
            return;
        }
        wishUpdater.updateWishWithAlarms(wish, wish.isOn());
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadWish();
    }
}
