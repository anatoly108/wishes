package com.wishesapp;

import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import com.wishesapp.models.Reminder;
import com.wishesapp.models.Wish;

import java.util.ArrayList;

public class WishFactory {
    public static Wish Create(){
        Wish wish = new Wish();
        wish.setOn(true);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        wish.setRingtoneUri(uri);

        return wish;
    }

    public static Wish CreateDefaultWish(SharedPreferences prefs, String defaultWishString){
        ArrayList<Reminder> reminders1 = new ArrayList<>();
        reminders1.add(RemindersFactory.Create(prefs,12, 0));

        Wish wish = new Wish();
        wish.setText(defaultWishString);
        wish.setReminders(reminders1);
        wish.setOn(false);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        wish.setRingtoneUri(uri);

        return wish;
    }
}
