package com.wishesapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.wishesapp.models.Reminder;
import com.wishesapp.models.Wish;
import com.wishesapp.repositories.IWishesRepository;

import java.util.Calendar;

public class WishUpdater {
    private IWishesRepository wishesRepository;
    private final AlarmManager alarmManager;
    private final Context appContext;
    private final Context context;

    public WishUpdater(IWishesRepository wishesRepository,
                       AlarmManager alarmManager,
                       Context appContext,
                       Context context) {

        this.wishesRepository = wishesRepository;
        this.alarmManager = alarmManager;
        this.appContext = appContext;
        this.context = context;
    }

    public void removeAlarms(Wish wish){
        for (Reminder reminder : wish.getReminders()) {
            Intent intent = new Intent(context, AlarmReceiver.class);
            PendingIntent oldPendingIntent = PendingIntent.getBroadcast(appContext,
                    (int) reminder.getId(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(oldPendingIntent);
        }
    }

    public void updateWishWithAlarms(Wish wish){
        updateWishWithAlarms(wish, true);
    }

    public void updateWishWithAlarms(Wish wish,
                                     boolean isOn) {
        wish.setOn(isOn);
        wishesRepository.saveWish(wish);

        // update alarms
        for (Reminder reminder : wish.getReminders()) {

            Intent intent = new Intent(context, AlarmReceiver.class);

            // taken from https://stackoverflow.com/questions/23751294/set-repeating-alarm-every-day-at-specific-time-in-android
            // 1. remove old alarm
            PendingIntent oldPendingIntent = PendingIntent.getBroadcast(appContext,
                (int) reminder.getId(),
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(oldPendingIntent);

            if(!isOn)
                continue;

            // 2. set new alarm
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, reminder.getHour());
            calendar.set(Calendar.MINUTE, reminder.getMinute());

            // shift to 1 day to prevent alarm right after setting it
            if(Calendar.getInstance().after(calendar)){
                // Move to tomorrow
                calendar.add(Calendar.DATE, 1);
            }

            intent.putExtra(Constants.wishText, wish.getText());
            intent.putExtra(Constants.wishId, wish.getId().toString());
            intent.putExtra(Constants.reminderId, reminder.getId());
            intent.putExtra(Constants.wishRingtone, wish.getRingtoneUri().toString());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext,
                    (int) reminder.getId(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),
                    24*60*60*1000,
                    pendingIntent);
        }
    }
}
