package com.wishesapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import com.wishesapp.activities.EditWishActivity;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String textContent = intent.getStringExtra(Constants.wishText);
        String wishId = intent.getStringExtra(Constants.wishId);
        String ringtoneUri = intent.getStringExtra(Constants.wishRingtone);
        long reminderId = intent.getLongExtra(Constants.reminderId, -1);

        Intent tapIntent = new Intent(context, EditWishActivity.class);
        tapIntent.putExtra(Constants.wishId, wishId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, tapIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, Constants.channelId)
            .setSmallIcon(R.drawable.large_icon_with_round)
            .setSound(Uri.parse(ringtoneUri))
            .setContentText(textContent)
            .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(textContent))
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify((int) reminderId, notification);
    }
}
