package com.wishesapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.wishesapp.activities.EditWishActivity;
import com.wishesapp.models.Wish;

import java.util.List;

class WishViewHolder extends RecyclerView.ViewHolder {
    TextView wishRemindersText;
    SwitchCompat wishOnOff;
    TextView wishText;

    WishViewHolder(View itemView) {
        super(itemView);

        wishText = itemView.findViewById(R.id.wish_text);
        wishRemindersText = itemView.findViewById(R.id.wish_reminders_text);
        wishOnOff = itemView.findViewById(R.id.wish_on_off);
    }

}

public class WishesAdapter extends RecyclerView.Adapter<WishViewHolder> {
    private List<Wish> wishes;
    private WishUpdater wishUpdater;
    private Context context;

    public WishesAdapter(List<Wish> wishes,
                         WishUpdater wishUpdater, Context context) {
        this.wishes = wishes;
        this.wishUpdater = wishUpdater;
        this.context = context;
    }

    @NonNull
    @Override
    public WishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_card,
                parent,
                false);
        return new WishViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final WishViewHolder holder, int position) {
        final Wish wish = wishes.get(position);
        holder.wishText.setText(wish.getText());
        String noReminders = context.getResources().getString(R.string.no_reminders);
        holder.wishRemindersText.setText(wish.getRemindersString(noReminders));
        holder.wishOnOff.setChecked(wish.isOn());

        holder.wishOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wishUpdater.updateWishWithAlarms(wish, isChecked);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editWishIntent = new Intent(context, EditWishActivity.class);
                editWishIntent.putExtra(Constants.wishId, wish.getId().toString());
                context.startActivity(editWishIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(wishes == null){
            return 0;
        } else{
            return wishes.size();
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
