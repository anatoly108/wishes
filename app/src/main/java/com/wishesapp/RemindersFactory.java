package com.wishesapp;

import android.content.SharedPreferences;
import com.wishesapp.models.Reminder;

public class RemindersFactory {
    public static Reminder Create(SharedPreferences prefs, int hour, int minute) {
        int currentId = prefs.getInt(Constants.reminderIdKey, 0);
        currentId += 1;

        Reminder reminder = new Reminder(currentId, hour, minute);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Constants.reminderIdKey, currentId);
        editor.apply();

        return reminder;
    }
}
